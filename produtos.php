<?php

require_once 'wp-load.php';

/* ========================================================================================================== */
/* ============================================ PRODUTOS ANTIGOS ============================================ */
/* ========================================================================================================== */
$produtos_json = file_get_contents('http://shoppingdostapetes.com.br/novo/apiprodutos.php');
$produtos_antigos_decode = json_decode($produtos_json);

$produtos_antigos = array();
foreach ($produtos_antigos_decode as $produto_antigo) {
	$cats = array();
	foreach ($produto_antigo->categoria as $categoria_antiga) {
		$cats[] = $categoria_antiga->name;
	}
	$produtos_antigos[$produto_antigo->titulo] = $cats;
}

echo '<pre style="width: 30%;display: inline-block;">';
echo '<p>================== PRODUTOS ANTIGOS ($produtos_antigos) ==================</p>';
echo print_r($produtos_antigos);
echo '</pre>';

/* =========================================================================================================== */
/* ============================================ /PRODUTOS ANTIGOS ============================================ */
/* =========================================================================================================== */

/* ======================================================================================================== */
/* ============================================ PRODUTOS NOVOS ============================================ */
/* ======================================================================================================== */
$produto_novo_query = new WP_Query(
	array(
		'post_type' => 'product',
		'posts_per_page' => -1
	)
);
$produtos_novos = array();
if ($produto_novo_query->have_posts()) {
	while ($produto_novo_query->have_posts()) { $produto_novo_query->the_post();
		$produtos_novos[] = array(
			'id' => get_the_ID(),
			'titulo' => get_the_title()
		);
	}
}

echo '<pre style="width: 30%;display: inline-block;">';
echo '<p>================== PRODUTOS NOVOS ($produtos_novos) ==================</p>';
echo print_r($produtos_novos);
echo '</pre>';

/* ========================================================================================================= */
/* ============================================ /PRODUTOS NOVOS ============================================ */
/* ========================================================================================================= */

/* ========================================================================================================== */
/* ============================================ CATEGORIAS NOVAS ============================================ */
/* ========================================================================================================== */
$categorias_novas_query = get_terms(
	array(
		'taxonomy' => 'product_cat',
		'hide_empty' => false,
	)
);
$categorias_novas = array();
foreach ($categorias_novas_query as $categorias) {
	$categorias_novas[$categorias->name] = $categorias->slug;
}

echo '<pre style="width: 30%;display: inline-block;">';
echo '<p>================== CATEGORIAS NOVAS ($categorias_novas) ==================</p>';
echo print_r($categorias_novas);
echo '</pre>';

/* =========================================================================================================== */
/* ============================================ /CATEGORIAS NOVAS ============================================ */
/* =========================================================================================================== */


foreach ($produtos_antigos as $titulo_produto_antigo => $produto_antigo) {
	foreach ($produtos_novos as $produto_novo) {
		if ($titulo_produto_antigo == $produto_novo['titulo']) {
			wp_set_object_terms( $produto_novo['id'], $produto_antigo, 'product_cat' );
		}
		// sleep(1);
	}
}