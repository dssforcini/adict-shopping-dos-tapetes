<?php // Template Name: Página Sobre Nós ?>
<?php get_header(); ?>
<section class="sobre-nos sobre-nos-titulo">
	<div class="col-full">
		<div class="titulo">
			<?php the_field('opcao_sobre_nos_titulo', 'options'); ?>
		</div>
		<div class="imagem" style="background-image: url('<?php the_field('opcao_sobre_nos_imagem', 'options'); ?>');"></div>
	</div>
</section>
<section class="sobre-nos sobre-nos-conteudo">
	<div class="col-full">
		<div class="titulo-loja">
			<h4 class="texto"><?php echo get_bloginfo('name'); ?></h4>
		</div>
		<div class="conteudo-divisao">
			<div class="conteudo-esquerdo">
				<div class="descricao">
					<?php the_field('opcao_sobre_nos_descricao', 'options'); ?>
				</div>
				<div class="tour-virtual">
					<div class="titulo-tour">
						<a href="http://shoppingdostapetes.com.br/tourvirtual/" target="_blank">
							<p class="titulo">
								<img src="<?php echo TEMA_URL.'/images/imagem-360.png'; ?>" alt="Fotos 360º">
								<span class="titulo"><?php the_field('opcao_titulo_tour_virtual', 'options'); ?></span>
							</p>
						</a>
					</div>
					<div class="iframe">
						<iframe src="http://shoppingdostapetes.com.br/tourvirtual/" frameborder="0" width="100%" height="300px"></iframe>
					</div>
				</div>
				<div class="endereco">
					<div class="titulo">
						<p><?php the_field('opcao_titulo_endereco', 'options'); ?></p>
					</div>
					<div class="bloco-endereco">
						<div class="icone">
							<img src="<?php echo TEMA_URL.'/images/map-pointer.svg'; ?>" alt="">
						</div>
						<div class="texto">
							<?php the_field('opcao_sobre_nos_endereco', 'options'); ?>					
						</div>
						<div class="botao">
							<a href="<?php the_field('opcao_link_google_maps', 'options'); ?>" target="_blank"><?php the_field('opcao_botao_google_maps', 'options'); ?></a>
						</div>
					</div>
				</div>
			</div>
			<div class="conteudo-direito">
				<div class="pilares">
					<?php if (have_rows('opcoes_sobre_nos_pilares', 'options')) { ?>
						<?php while (have_rows('opcoes_sobre_nos_pilares', 'options')) { the_row(); ?>
							<div class="pilar">
								<div class="titulo"><h4 class="texto"><?php the_sub_field('opcao_pilarares_titulo'); ?></h4></div>
								<div class="descricao"><?php the_sub_field('opcao_pilarares_descricao'); ?></div>
							</div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="sobre-nos sobre-nos-veja-tambem">
	<div class="col-full">
		<h3 class="titulo-veja-tambem">Veja Também</h3>
		<?php echo do_shortcode('[products limit="4" columns="4" visibility="featured"]'); ?>
		<a href="<?php echo home_url('loja'); ?>" class="botao-ver-produtos">VER TODOS OS PRODUTOS</a>
	</div>
</section>
<?php get_footer(); ?>