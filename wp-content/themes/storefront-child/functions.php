<?php

define ('TEMA_URL', get_stylesheet_directory_uri());

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

add_action( 'init', 'custom_remove_footer_credit', 10 );

add_filter('show_admin_bar', '__return_false');

function custom_remove_footer_credit () {
    remove_action( 'storefront_footer', 'storefront_credit', 20 );
    // add_action( 'storefront_footer', 'custom_storefront_credit', 20 );
}

if( function_exists('acf_add_options_page') ) {
 	
 	// add parent
	$parent = acf_add_options_page(array(
		'page_title' => 'Opções do Tema',
		'menu_title' => 'Opções do Tema',
		'menu_slug' => 'opcoes-tema',
		'redirect' => true
	));
	
	
	// add sub page
	acf_add_options_sub_page(array(
		'page_title' => 'Geral',
		'menu_title' => 'Geral',
		'parent_slug' => $parent['menu_slug'],
	));
	
	
	// add sub page
	acf_add_options_sub_page(array(
		'page_title' => 'Página Sobre Nós',
		'menu_title' => 'Página Sobre Nós',
		'parent_slug' => $parent['menu_slug'],
	));
	
}

if (!is_admin()) {
	add_filter( 'woocommerce_get_price_html', 'wpa83367_price_html', 100, 2 );
	function wpa83367_price_html( $price, $product ){
		if (is_single()) {
			if ($product->is_on_sale()) {
				return str_replace( '<ins>', ' <ins><span class="pref-price-now">Por:</span> ', str_replace('<del>', '<del><span class="pref-price-old">De:</span> ', str_replace('</ins>', '<span class="sufix-price"> à vista</span></ins>', $price)) );
			}	
		} else {
			return $price;
		}
	}
}

add_filter('woocommerce_variable_price_html', 'custom_variation_price', 10, 2);
function custom_variation_price( $price, $product ) { 
	if (is_single()) {
     $price = '<span>A partir de: </span>';
     $price .= '<span class="valor-minimo">'.wc_price($product->get_price()).'</span>'; 
 	} else {
 		$price = '<span class="valor-minimo">'.wc_price($product->get_price()).'</span>';
 	}
    return $price;
}

/* custom_woocommerce_template_loop_add_to_cart */
add_filter( 'woocommerce_product_add_to_cart_text' , 'custom_woocommerce_product_add_to_cart_text' );
function custom_woocommerce_product_add_to_cart_text() {
	global $product;
	
	$product_type = $product->product_type;
	
	switch ( $product_type ) {
		case 'external':
			return __( 'COMPRAR', 'woocommerce' );
		break;
		case 'grouped':
			return __( 'COMPRAR', 'woocommerce' );
		break;
		case 'simple':
			return __( 'ADICIONAR', 'woocommerce' );
		break;
		case 'variable':
			return __( 'COMPRAR', 'woocommerce' );
		break;
		default:
			return __( 'VER MAIS', 'woocommerce' );
	}
	
}

add_action(	'woocommerce_share', 'woocommerce_compartilhar');
function woocommerce_compartilhar() {
	echo '<div class="box-sob-medida">';
		echo '<div class="botao-sob-medida">';
			echo '<span class="sob-medida">Sob Medida</span><span class="para-adquirir">Para adquirir entre em contato:</span>';
		echo '</div>';
		echo '<div class="dados">';
			echo '<div class="telefones">';
				if (have_rows('opcao_telefones', 'options')) {
					while (have_rows('opcao_telefones', 'options')) { the_row();
						echo '<div class="telefone item">';
							echo '<a href="tel:'.preg_replace("/[^0-9]/", "", get_sub_field('opcao_telefone')).'">';
								echo '<span class="telefone '.((get_sub_field('opcao_telefone_whatsapp')) ? 'whatsapp' : '').' item-icone">';
									if (get_sub_field('opcao_telefone_whatsapp')) {
										echo '<img src="'.TEMA_URL.'/images/whatsapp-logo.svg" class="whatsapp" alt="WhatsApp">';
									} else {
										echo '<img src="'.TEMA_URL.'/images/telephone.svg" alt="Telefone/Celular">';
									}
								echo '</span>';
								echo '<span class="item-texto">'.get_sub_field('opcao_telefone').'</span>';
							echo '</a>';
						echo '</div>';
					}
				}
			echo '</div>';
			echo '<div class="skype-formulario">';
				echo '<div class="item">';
					echo '<a href="javascript:void(0);">';
						echo '<span class="skype item-icone">';
							echo '<img src="'.TEMA_URL.'/images/skype.svg" alt="Skype">';
						echo '</span>';
						echo '<span class="item-texto">'.get_field('opcao_skype', 'options').'</span>';
					echo '</a>';
				echo'</div>';
				echo '<div class="item">';
					echo '<a href="javascript:void(0);">';
						echo '<span class="formulario item-icone">@</span>';
						echo '<span class="item-texto">Formulário de contato</span>';
					echo '</a>';
				echo'</div>';
			echo'</div>';
		echo'</div>';
	echo '</div>';
	echo '<div class="horario-atendimento">';
		echo get_field('opcao_horario_atendimento', 'options');
	echo'</div>';
}


if ( ! function_exists( 'storefront_cart_link' ) ) {
	/**
	 * Cart Link
	 * Displayed a link to the cart including the number of items present and the cart total
	 *
	 * @return void
	 * @since  1.0.0
	 */
	function storefront_cart_link() {
		?>
			<a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'storefront' ); ?>">
				<span class="count"><?php echo wp_kses_data( sprintf( _n( '%d item', '%d products', WC()->cart->get_cart_contents_count(), 'storefront' ), WC()->cart->get_cart_contents_count() ) ); ?></span>
			</a>
		<?php
	}
}