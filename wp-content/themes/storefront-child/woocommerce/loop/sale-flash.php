<?php
/**
 * Product loop sale flash
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/sale-flash.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post, $product;

?>
<?php
	$cats = get_the_terms(get_the_ID(), 'product_cat');
	$is_lancamento = false;
	$lancamento_texto = '';
	// echo '<pre>';
	// echo print_r($cats);
	// echo '</pre>';
	foreach ($cats as $cat) {
		if ($cat->slug == 'lancamentos') { // se o produto do loop conter a categoria 82 (lancamento)
			$is_lancamento = true;
			$lancamento_texto = $cat->name;
		}
	}
?>
<?php if ($is_lancamento) { ?>
	<?php /* ?><span class="label-lancamento"><?php echo $lancamento_texto; ?></span><?php */ ?>
	<span class="label-lancamento">LANÇAMENTO</span>
<?php } ?>
<?php // if ( $product->is_on_sale() ) : ?>

	<?php // echo apply_filters( 'woocommerce_sale_flash', '<span class="onsale">' . esc_html__( 'Sale!', 'woocommerce' ) . '</span>', $post, $product ); ?>

<?php // endif;

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
