<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) { ?>
	
	<div class="abas">

		<div class="woocommerce-tabs wc-tabs-wrapper">
			<ul class="tabs wc-tabs" role="tablist">
				<?php foreach ( $tabs as $key => $tab ) : ?>
					<li class="<?php echo esc_attr( $key ); ?>_tab" id="tab-title-<?php echo esc_attr( $key ); ?>" role="tab" aria-controls="tab-<?php echo esc_attr( $key ); ?>">
						<a href="#tab-<?php echo esc_attr( $key ); ?>"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?></a>
					</li>
				<?php endforeach; ?>
			</ul>
			<?php foreach ( $tabs as $key => $tab ) : ?>
				<div class="woocommerce-Tabs-panel woocommerce-Tabs-panel--<?php echo esc_attr( $key ); ?> panel entry-content wc-tab" id="tab-<?php echo esc_attr( $key ); ?>" role="tabpanel" aria-labelledby="tab-title-<?php echo esc_attr( $key ); ?>">
					<?php if ( isset( $tab['callback'] ) ) { call_user_func( $tab['callback'], $key, $tab ); } ?>
				</div>
			<?php endforeach; ?>

			<div class="contato">
				
				<div class="compartilhe">
					<p>
						<span>Compartilhe este produto:</span>
						<a style="text-decoration: none;" href="https://twitter.com/intent/tweet/?url=<?php the_permalink(); ?>&amp;text=<?php the_title(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php echo TEMA_URL.'/images/produto-compartilhar-twitter.png'; ?>" alt="Twitter" title="Compartilhe no Twitter"></a>
						<a href="https://wa.me/?text=<?php the_permalink(); ?>" target="_blank"><img src="<?php echo TEMA_URL.'/images/produto-compartilhar-whatsapp.png'; ?>" alt="WhatsApp" title="Compartilhe no WhatsApp"></a>
						<a style="text-decoration: none;" href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php echo TEMA_URL.'/images/produto-compartilhar-facebook.png'; ?>" alt="Facebook" title="Compartilhe no Facebook"></a>
					</p>
				</div>
				<div class="nao-encontrou">
					<div class="titulo">Não encontrou o produto que estava procurando?</div>
					<div class="subtitulo">Podemos ajudar! Fale Conosco.</div>
					<?php woocommerce_compartilhar(); ?>
				</div>

			</div>

		</div>

		<section class="produtos-veja-tambem lateral-aba">
			<h3 class="titulo-veja-tambem">Veja Também</h3>
			<?php echo do_shortcode('[products limit="4" columns="2" visibility="featured"]'); ?>
			<a href="<?php echo home_url('loja'); ?>" class="botao-ver-produtos">Ver Mais</a>
		</section>

	</div>

<?php } else { ?>
	<section class="produtos-veja-tambem">
		<h3 class="titulo-veja-tambem">Veja Também</h3>
		<?php echo do_shortcode('[products limit="4" columns="4" visibility="featured"]'); ?>
		<a href="<?php echo home_url('loja'); ?>" class="botao-ver-produtos">Ver Mais</a>
	</section>
<?php } ?>
