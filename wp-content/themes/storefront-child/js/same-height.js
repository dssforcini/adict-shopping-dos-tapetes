var globalElemento;
var globalQtdItensLinha;
var globalQtdItensLinhaMobile = 0;

/* === ELEMENTO === */
function setGlobalElemento(element) {
	globalElemento = element;
	console.log('GlobalElemento: '+globalElemento);
}
function getGlobalElemento() {
	return globalElemento;
}
/* === /ELEMENTO === */

/* === QTD ITENS LINHA === */
function setGlobalQtdItensLinha(qtd) {
	globalQtdItensLinha = qtd;
	console.log('GlobalQtdItensLinha: '+globalQtdItensLinha);
}
function getGlobalQtdItensLinha() {
	return globalQtdItensLinha;
}
/* === /QTD ITENS LINHA === */

/* === QTD ITENS LINHA MOBILE === */
function setGlobalQtdItensLinhaMobile(qtd) {
	if (qtd != '') {
		globalQtdItensLinhaMobile = qtd;
	} else {
		globalQtdItensLinhaMobile = 2;
	}
	console.log('GlobalQtdItensLinhaMobile: '+globalQtdItensLinhaMobile);
}
function getGlobalQtdItensLinhaMobile() {
	return globalQtdItensLinhaMobile;
}
/* === /QTD ITENS LINHA MOBILE === */


function mesmaAltura(elemento, qtdItensLinha, qtdItensLinhaMobile = '') {
	if (elemento.length) {
		console.clear();
		console.log('Same Height Loaded');

		setGlobalElemento(elemento);
		setGlobalQtdItensLinha(qtdItensLinha);
		setGlobalQtdItensLinhaMobile(qtdItensLinhaMobile);

		var contador = 1, contadorLinha = 1;
		var nomeDaClasse = elemento.substring(1, elemento.length);
		var qtdItensLinha_interno = 0;

		if (jQuery(window).width() >= 992) {
			qtdItensLinha_interno = getGlobalQtdItensLinha();
		} else {
			qtdItensLinha_interno = getGlobalQtdItensLinhaMobile();
		}

		jQuery(elemento).each(function(index, el) {

			// verifica onde começa e termina uma linha
			if (contador > qtdItensLinha_interno) { contadorLinha++; contador = 1; }

			// limpa todas as classe e adiciona a classe principal que é o elemento
			// jQuery(this).removeClass();
			for (var iCont = 1; iCont <= 99; iCont++) { jQuery(this).removeClass(nomeDaClasse+'__line_'+iCont); }
			if (!jQuery(this).hasClass(nomeDaClasse)) { jQuery(this).addClass(nomeDaClasse); }

			// adiciona outra classe baseada na classe principal do elemento
			jQuery(this).addClass(nomeDaClasse+'__line_'+contadorLinha);
			contador++;
		});

		// percorre cada linha que foi encontrada de acordo com o número de elementos
		for (var i = 1; i <= contadorLinha; i++) {
			let alturaMaxima = 0;

			// percorre cada elemento da linha do loop atual
			$(elemento+'__line_'+i).each(function(index, el) {

				// limpa o estilo do elemento
				jQuery(this).removeAttr('style');

				// verifica se a altura do elemento atual é maior que a altura de todos os 
				// elementos da linha, se for, pega a altura dele e seta como 
				// altura máxima de todos os elementos vizinhos
				if ($(this).height() > alturaMaxima) {
					alturaMaxima = $(this).height();
				}
			});

			// seta a altura pra todos os elementos da linha com a maior altura encontrada
			$(elemento+'__line_'+i).height(alturaMaxima);
		}
	}
}

jQuery(window).on('resize', function(event) {
	mesmaAltura(globalElemento, globalQtdItensLinha, globalQtdItensLinhaMobile);
});