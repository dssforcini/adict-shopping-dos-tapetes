<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="col-full">

			<?php
			/**
			 * Functions hooked in to storefront_footer action
			 *
			 * @hooked storefront_footer_widgets - 10
			 * @hooked storefront_credit         - 20
			 */
			do_action( 'storefront_footer' );
			?>

		</div><!-- .col-full -->
	</footer><!-- #colophon -->
	<div class="coluna-footers-dev">
		<div class="col-full">
		    <div class="site-info">
		        <div class="col-2 direitos">
		        	<strong>Shopping dos Tapetes</strong> | Todos os direitos reservados.
		        </div>
		        <div class="col-2 desenvolvido">
		        	<span>DESENVOLVIDO POR:</span><a href="https://adict.com.br" target="_blank"><img src="<?php echo home_url('wp-content/uploads/2019/08/adict.png'); ?>" alt="ADICT"></a>
		        </div>
		    </div><!-- .site-info -->
		</div>
	</div>
	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
